%Planta(Codigo, Nome, Origem, Raiz, Habitat)

planta(1, oncidium, america_do_sul, fina, epifita).
planta(1, oncidium, america_do_norte, fina, epifita).
planta(2, cattlyea, america_do_sul, grossa, epifita).
planta(3, vanda, asia, grossa, aerea).
planta(3, vanda, asia, grossa, epifita).
planta(4, laelia, america_do_sul, grossa, epifita).
planta(4, laelia, america_central, grossa, epifita).
planta(5, milt�nia, america_do_sul, fina, epifita).
planta(5, milt�nia, america_central, fina, epifita).
planta(6, phalaenopsis, asia, grossa, epifita).
planta(7, paphiopedilum, asia, fina, epifita).
planta(7, paphiopedilum, asia, fina, terrestre).

%Flor(Codigo, Tamanho, Quantidade, Haste Floral, Esta��o, Dura��o)

flor(1, mini, muitas, cacho, inverno, 30).
flor(1, mini, muitas, cacho, ver�o, 30).
flor(1, mini, muitas, cacho, outono, 30).
flor(1, mini, muitas, cacho, primavera, 30).
flor(2, gigante, poucas, espata, inverno, 20).
flor(2, gigante, poucas, espata, ver�o, 20).
flor(2, gigante, poucas, espata, outono, 20).
flor(3, media, media, cacho, inverno, 30).
flor(3, media, media, cacho, ver�o, 30).
flor(3, media, media, cacho, outono, 30).
flor(3, media, media, cacho, primavera, 30).
flor(4, grande, muitas, cacho, inverno, 20).
flor(4, grande, muitas, cacho, ver�o, 20).
flor(4, grande, muitas, cacho, outono, 20).
flor(5, pequena, pouca, cacho, ver�o).
flor(5, pequena, pouca, cacho, primavera).
flor(6, mini, media, cacho, inverno, 45).
flor(6, pequena, media, cacho, inverno, 45).
flor(6, media, media, cacho, inverno, 45).
flor(6, mini, media, cacho, primavera, 45).
flor(6, pequena, media, cacho, primavera, 45).
flor(6, media, media, cacho, primavera, 45).
flor(6, mini, media, cacho, ver�o, 45).
flor(6, pequena, media, cacho, ver�o, 45).
flor(6, media, media, cacho, ver�o, 45).
flor(6, mini, media, cacho, outono, 45).
flor(6, pequena, media, cacho, outono, 45).
flor(6, media, media, cacho, outono, 45).
flor(7, grande, poucas, espata, inverno, 30).
flor(7, pequena, poucas, espata, inverno, 30).
flor(7, grande, poucas, espata, primavera, 30).
flor(7, pequena, poucas, espata, primavera, 30).
flor(7, grande, poucas, espata, ver�o, 30).
flor(7, pequena, poucas, espata, ver�o, 30).
flor(7, grande, poucas, espata, outono, 30).
flor(7, pequena, poucas, espata, outono, 30).

%Folhas(Codigo, Espessura, Comprimento, elasticidade, Colora��o)

folhas(1, fina, longa, flexivel, escura).
folhas(2, larga, media, rigida, escura).
folhas(3, fina, longa, rigida, escura).
folhas(4, fina, longa, rigida, escura).
folhas(5, fina, longa, flexivel, clara).
folhas(6, larga, media, rigida, escura).
folhas(7, fina, curta, flexivel, escura).

pergunta(A,B,C,D,E,F,G,H,I,J,K,L,Z) :- planta(X,Z,A,B,C),flor(X,D,E,F,G,H),folhas(X,I,J,K,L).


inicio :- write('Qual a origem de sua orquidea(america_do_sul,america_do_norte,america_central,asia) '),nl,

	read(A),



	write('Qual � espessura da raiz(gro�a,fina)'),nl,
	read(B),



	write('Qual o habitat da planta(epifita,terrestre,aerea)'),nl,
	read(C),

	write('Qual o tamanho da flor(mini,media,grande,gigante)'),nl,
	read(D),


	write('Quantas flores(poucas,media,muitas)'),nl,
	read(E),


	write('Haste floral(cacho,espata)'),nl,
	read(F),

	write('Esta��o do ano a florescer(inverno,verao,primavera,outono)'),nl,
	read(G),


	write('Tempo de dura�ao da flor em dias(20,30,45)'),nl,
	read(H),

	write('Espessura das folhas(fina,larga)'),nl,
	read(I),

	write('Comprimento das folhas(longa,media,cura)'),nl,
	read(J),

	write('Elasticidade das folhas(rigida,flexivel)'),nl,
	read(K),

	write('Colora��o das folhas(clara,escura)'),nl,
	read(L),
	pergunta(A,B,C,D,E,F,G,H,I,J,K,L,Z),
	write(Z),nl,fail.




